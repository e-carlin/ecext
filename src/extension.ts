import * as vscode from "vscode";

export function activate(context: vscode.ExtensionContext) {
  push("extension.openTerm", () => {
    vscode.commands.executeCommand("workbench.action.toggleMaximizedPanel");
    vscode.commands.executeCommand("workbench.action.terminal.toggleTerminal");
  });

  function push(n: string, c: any) {
	  context.subscriptions.push(vscode.commands.registerCommand(n, c));
  }
}

// this method is called when your extension is deactivated
export function deactivate() {}
