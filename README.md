# ecext
A vscode extension to manage things I'd like to do in vscode.

My intention is to bundle up actions I'd like to take in vscode that are not
easily configured through the normal routes (ex settings.json). An example
is for building macros. Currently there is no way to build macros without using
another extension.